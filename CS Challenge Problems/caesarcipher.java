package cipher;

public class Caesercipher {
	
	public static String encrypt(String plainText, int shift) {
		
		if(shift>26) {		// The number cannot be greater than 26 or less than 0
			shift = 0;
		}
		else if (shift<0) {
			shift = 0;
		}
		
		String enterText = "";
		int length = plainText.length();
		
		for(int i = 0; i<length; i++) {
			char ch = plainText.charAt(i);
			if(Character.isLetter(ch)) {
				if(Character.isLowerCase(ch)) {
					char c = (char)(ch+shift);
					if(c>'z') {
						enterText += (char)(ch - (26-shift));	// This loops the number back to the beginning of the alphabet if it reaches the end
					}
					else {
						enterText += c;
					}
				}
				else if(Character.isUpperCase(ch)) {	// Same process for uppercase characters
					char c = (char)(ch+shift);
					if(c>'Z') {
						enterText += (char)(ch - (26-shift));
					}
					else {
						enterText += c;
					}
				}
			}
			else {
				enterText += ch;
			}
		}

		return enterText;
	}

	public static void main(String[] args) {
	
		String encryptThis = "The quick brown fox jumped over the lazy dogs numbers and other things 1, 2, and 3!";		// Enter text here
		String cipher = encrypt(encryptThis, 1);	// Enter shift number here
		System.out.println(cipher);

	}

}
