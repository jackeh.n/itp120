public class Duck extends Animal {
    int size;

    public Duck() {
        super();
        System.out.println("quack quack");
        size = 24;
    }
    
    public Duck(int duckSize) {
        super();
        size = duckSize;
        System.out.println("Size is " + size);
    }
}
