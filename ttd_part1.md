1. What are the three laws of test driven development?
    1. Only write production code to pass a failing test
    2. Write only enough of a test to demonstrate a failure.
    3. Write only enough code to pass the test

2. Explain the Red, Green, Refactor process.
    The Red, Green, Refactor process is a formula to test code.
    
3. What are the three characteristics of rotting code?
    The characteristics of rotting code are rigidity, fragility, immobility.

4. Explain how fear promotes code rot and why fear exists in the first place. How does TDD break this vicious cycle?
    Without tests, the entire program must be modified to fix one problem, which could lead to many others. TDD allows the programmer to make small adjustments using tests to verify that the code still completes all of it's objectives.

5. Uncle Bob mentions FitNesse as an example of a product that uses TDD effectively. What is FitNesse? What does it do?
    FitNesse is an automatic testing program.

6. What does Uncle Bob say about a program with a long bug list? What does he say it comes from?
    According to Uncle Bob, a program with a long bug list comes from irresponsibility are and carelessness,

7. What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?
    The other benefits of TDD are improved design and complete and reliable low level documentation.
